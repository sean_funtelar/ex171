﻿using System;

namespace week3_ex17_pt2
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Can you guess the name I'm thinking of?");
            Console.WriteLine("Take a guess");
            
            var name = Console.ReadLine();

            if(name == "Kadin")
                Console.WriteLine("Yes that's the name I was thinking of!");

            else
                Console.WriteLine("No that's not the name I was thinking of.");
                
        }
    }
}